//
//  MasterViewController.m
//  Cake List
//
//  Created by Stewart Hart on 19/05/2015.
//  Copyright (c) 2015 Stewart Hart. All rights reserved.
//

#import "MasterViewController.h"
#import "CakeCell.h"
#import "CakeItem.h"
#import "NetworkFetchingManager.h"
#import "Reachability.h"
#import <UIKit/UIKit.h>

@interface MasterViewController ()
@property (strong, atomic) NSMutableArray *returnedCakeObjects;
@property (nonatomic, strong) NSCache *imageCache;
@property (nonatomic, copy) void (^completionHandler)(void);



@end

@implementation MasterViewController



/// define a BOOL variable reflecting current reachability of internet
-(BOOL)reachable {
  Reachability *r = [Reachability reachabilityWithHostName:@"www.google.com"];
  NetworkStatus internetStatus = [r currentReachabilityStatus];
  if(internetStatus == NotReachable) {
    return NO;
  }
  return YES;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  
  UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
  [refreshControl addTarget:self action:@selector(refreshData:) forControlEvents:UIControlEventValueChanged];
  
  [self.tableView addSubview:refreshControl];
  
  [self getCakeDataFromURL];
  [self checkReachabilityAndShowAlertIfNone];
  
  self.imageCache = [[NSCache alloc]init];
  [self.imageCache setDelegate:self];
  NSLog(@"%@",[_imageCache delegate]);
  
}

- (void) checkReachabilityAndShowAlertIfNone
{
  dispatch_async(dispatch_get_main_queue(), ^{
    if (self.reachable == YES) {
      NSLog(@"YES");
    } else {
      NSLog(@"NO");
      UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Internet!"
                                                      message:@"You are not connected, go online for updated recipies..."
                                                     delegate:self
                                            cancelButtonTitle:@"OK"
                                            otherButtonTitles: nil];
      [alert show];
      
    }
  });
}


- (void)refreshData : (id)sender
{
  dispatch_queue_t backgroundQueue;
  backgroundQueue = dispatch_queue_create("com.recipies.backqueue", NULL);
  dispatch_async(backgroundQueue, ^(void){
    
    [self getCakeDataFromURL];
    
    dispatch_async(dispatch_get_main_queue(), ^{
      // UI updates in main queue
      [self.tableView reloadData];
      [sender endRefreshing];
      
    });
  });
}


- (void)reloadData : (id)sender
{
  // Reload table data
  [self.tableView reloadData];
  
  // End the refreshing
  [sender endRefreshing];
}

#pragma mark - Table View Delegate Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
  return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  return self.returnedCakeObjects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  CakeCell *cell = (CakeCell*)[tableView dequeueReusableCellWithIdentifier:@"CakeCell"];
  
  if (self.returnedCakeObjects != nil) {
    
    CakeItem *aCake = self.returnedCakeObjects[indexPath.row];
    
    cell.titleLabel.text = aCake.cakeTitle;
    
    cell.descriptionLabel.text = aCake.cakeDescription;
    
    NSURL *imageURL = [NSURL URLWithString: aCake.cakePictureURL];
    
    UIImage *image = [UIImage imageNamed:@"loadingImage.png"];
    cell.cakeImageView.image = image;
    
    if ([self.imageCache objectForKey: imageURL] != nil) {
      NSLog(@"ERM");
      cell.cakeImageView.image = [self.imageCache objectForKey: imageURL];
    } else {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
      NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
      if (imageData){
        dispatch_async(dispatch_get_main_queue(), ^{
          if (cell)
            cell.cakeImageView.image = [UIImage imageWithData:imageData];
          [self.imageCache setObject:[UIImage imageWithData:imageData] forKey:imageURL];
          
        });
      }
    });
      
    }
    
  }
  
  
  return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
  [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


- (void)getCakeDataFromURL{
  
  NetworkFetchingManager* mygetCakeDataFromURL = [[NetworkFetchingManager alloc] init];
  dispatch_queue_t backgroundQueue;
  backgroundQueue = dispatch_queue_create("com.recipies.backqueue", NULL);
  
  dispatch_async(backgroundQueue, ^(void){
    //background task
    
    NetworkFetchingManager* fetchManager = [[NetworkFetchingManager alloc] init];
    
    [fetchManager setCompletionHandler:^(NSError *error, NSArray *arrayOfReturnedCakes){
      if (error != nil) {
        [self checkReachabilityAndShowAlertIfNone];
        return;
      }
      if (arrayOfReturnedCakes != nil) {
        self.returnedCakeObjects = arrayOfReturnedCakes;
        // additional check and show error if not connected
        if (!self.reachable) {
          self.returnedCakeObjects = NULL;
          [self checkReachabilityAndShowAlertIfNone];
        }
        // meanwhile back on the main queue
        dispatch_async(dispatch_get_main_queue(), ^{
          [self.tableView reloadData];
        });
      }
      
      NSLog(@"Completion");
    }];
    
    // Network request for cake info json data with completion handler
    [fetchManager getCakeDataFromURL:@"https://gist.githubusercontent.com/hart88/198f29ec5114a3ec3460/raw/8dd19a88f9b8d24c23d9960f3300d0c917a4f07c/cake.json"];
    
  });
  
}

@end
