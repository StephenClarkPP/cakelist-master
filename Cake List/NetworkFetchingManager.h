//
//  NetworkFetchingManager.h
//  Cake List
//
//  Created by Stephen Clark on 22/11/2017.
//  Copyright © 2017 Stewart Hart. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetworkFetchingManager : NSObject

- (NSMutableArray *)getCakeDataFromURL:(NSString *)urlString;
@property (nonatomic, strong) NSURLSessionDataTask *sessionTask;
@property (nonatomic, copy) void (^completionHandler)(NSError *, NSMutableArray *);
@end
