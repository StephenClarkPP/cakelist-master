//
//  CakeItem.m
//  Cake List
//
//  Created by Stephen Clark on 22/11/2017.
//  Copyright © 2017 Stewart Hart. All rights reserved.
//

#import "CakeItem.h"

@implementation CakeItem
@synthesize cakePictureURL;
@synthesize cakeTitle;
@synthesize cakeDescription;
@synthesize cakePicture;
@end
