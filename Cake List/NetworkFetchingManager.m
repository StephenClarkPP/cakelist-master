//
//  NetworkFetchingManager.m
//  Cake List
//
//  Created by Stephen Clark on 22/11/2017.
//  Copyright © 2017 Stewart Hart. All rights reserved.
//

#import "NetworkFetchingManager.h"
#import "CakeItem.h"

@implementation NetworkFetchingManager


- (NSMutableArray *)getCakeDataFromURL:(NSString *)urlString {
  NSLog(@"getCakeDataFromURL");
  
  // NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString: urlString]];
  NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString: urlString] cachePolicy: NSURLRequestReloadIgnoringCacheData timeoutInterval: 15.00];
  
  // NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
  // sessionConfig.timeoutIntervalForRequest = 30.00;
  // sessionConfig.timeoutIntervalForResource = 30.00;

  
  _sessionTask = [[NSURLSession sharedSession] dataTaskWithRequest:request
                                                 completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                   NSLog(@"sessionTask Completion handler");
                                                   if (error != nil) {
                                                     NSLog(@"ERROR");
                                                     self.completionHandler(error, NULL);
                                                   } else {
                                                     
                                                     NSError *jsonError = nil;
                                                     
                                                     id responseData = [NSJSONSerialization
                                                                        JSONObjectWithData:data
                                                                        options:kNilOptions
                                                                        error:&jsonError];
                                                     if (!jsonError && !error ){
                                                       
                                                       NSArray* returnedCakeObjects = responseData;
                                                       
                                                       NSMutableArray *cakeArray = [[NSMutableArray alloc] initWithCapacity:100];
                                                       
                                                       // we will traverse through the returned items
                                                       for (id object in returnedCakeObjects) {
                                                         CakeItem *cakeItem = [[CakeItem alloc] init];
                                                         cakeItem.cakeTitle = (@"%@", object[@"title"]);
                                                         cakeItem.cakeDescription = (@"%@", object[@"desc"]);
                                                         cakeItem.cakePictureURL = (@"%@", object[@"image"]);
                                                         [cakeArray addObject:cakeItem];
                                                         
                                                       }
                                                       
                                                       //NSLog(@"%@", cakeArray);
                                                       if (self.completionHandler != nil)
                                                       {
                                                         self.completionHandler(error, cakeArray);
                                                         error = NULL;
                                                         cakeArray = NULL;
                                                         data = NULL;
                                                       }
                                                       
                                                       
                                                     } else {    self.completionHandler(error, NULL); }
                                                   }
                                                   
                                                 }];
  
  [self.sessionTask resume];
  
  return NULL;
}

@end


