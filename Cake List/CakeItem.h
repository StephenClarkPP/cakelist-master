//
//  CakeItem.h
//  Cake List
//
//  Created by Stephen Clark on 22/11/2017.
//  Copyright © 2017 Stewart Hart. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface CakeItem : NSObject
@property (nonatomic, strong) UIImage *cakePicture;
@property (nonatomic, strong) NSString *cakePictureURL;
@property (nonatomic, strong) NSString *cakeTitle;
@property (nonatomic, strong) NSString *cakeDescription;
@end


